# MobileWave #
Mobile application reading phone sensors data and streaming it as JSON-LD.

## Running the app ##

```
#!bash

cordova run <ios|android|browser>
```