// var stream = require('stream');

const SERVER = "http://mw-ysedera.rhcloud.com/";
const DATA_STREAM = [];
const TEMPLATE = 'template.json';

const transformer = new Transformer();


var logError = function (error) {
    console.log("Error Source " + error.source);
    console.log("Error Target " + error.target);
    console.log("Error Code" + error.code);
}
var readFileAsText = function (entry) {
    console.log("Download complete: " + entry.toURL());
    entry.file(function (file) {

        var reader = new FileReader();
        reader.onloadend = function () {
            transformer.context = JSON.parse(this.result);
            console.log(transformer.context.toString());
        };
        reader.readAsText(file);

    }, logError);
};


function Transformer() {
    /**
     * Transformer: class responsible of loading the context form the server into memory, transforming and publishing
     * json-ld to server(fuseki)
     * */
    this.context = {};
    this.loadContext = function () {
        /**
         * This function download content template from the internet and read the downloaded file and parse it to json
         * and keep it in memory(context)
         * */
        // Access to file system
        window.requestFileSystem(window.TEMPORARY, 5 * 1024 * 1024, function (fs) {
            var fsOption = {
                create: true,
                exclusive: false
            };
            // Get or create new file on the local file system
            fs.root.getFile(TEMPLATE, fsOption, function (entry) {
                var fileTransfer = new FileTransfer();
                var remoteURI = SERVER + '/template';
                var localURI = entry.toURL();
                // Download the file from the remote host and store on the local file system
                fileTransfer.download(remoteURI, localURI, readFileAsText, logError, true);
            }, logError);
        }, logError);
    };
    this.transform = function (sensorData, sensorType, callback) {
        /**
         * Given a JSON object, it's type (e.g. device, location) and context it transform it to JSON-LD
         * */

        var data = {}; // local variable to hold the formatted doc to be used as input for json-ld transformation
        var sensorContext = this.context[sensorType]; // local variable holding the appropriate context information
        // loop perform a pre-processing for the raw data to prepare it for json-ld transformation
        // eg. {"name":"some name"} => {"http://example.com/name":"some name"}
        for (var sensorProperty in sensorData) {
            console.log(sensorProperty);
            console.log(sensorData[sensorProperty]);
            if (sensorContext.hasOwnProperty(sensorProperty)) {
                data[sensorContext[sensorProperty]] = sensorData[sensorProperty];
            }
        }
        console.log(data.toString());
        console.log(sensorData.toString());
        // perform transformation to json-ld based on the context and the json data
        jsonld.compact(data, sensorContext, function (err, compacted) {
            if (err) {
                // log failure
                logError(err);
            }
            if (callback) {
                callback(sensorContext, compacted);
            }

        });
    };
    this.publish = function (dataStream, successCallback, errorCallback) {
        /**
         * POST dataStream array to predefined Server (TOBE: fuseki)
         * */
        $.ajax({
            type: "POST",
            url: SERVER,
            contentType: "application/x-www-form-urlencoded",
            data: JSON.stringify(dataStream),
            success: successCallback,
            error: errorCallback,
        });

    }
}

$(document).ready(function () {
    $(document).bind("deviceready", function () {
        console.log('Entering Device Reading !');
        // Bind event to buttons
        $(document).on("click", "#btn-save-device-info", saveDeviceInfo);
        $(document).on("click", "#btn-save-location-info", saveLocationInfo);
        $(document).on("click", "#btn-send", sendInfo);

        console.log('Leaving Device Reading !');
        // load context from remote host
        transformer.loadContext();
    });

    var storeAndDisplayMessages = function (ctx, msg) {
        /**
         * Update UI with the context and transformed json-ld
         * */
        DATA_STREAM.push(msg);
        $('#context').text(JSON.stringify(ctx, null, 2));
        $('#results').text($('#results').text() + JSON.stringify(msg, null, 2));
    };
    var saveDeviceInfo = function () {
        /**
         * Get an instance of the device info, perform transformation and store it in DATA_STREAM
         * */
        console.log('Saving Device Info!');
        transformer.transform(device, 'device', storeAndDisplayMessages)
    };
    var saveLocationInfo = function () {
        /**
         * Get an instance of the navigator, fetch the current location, transform and store current location info
         * in DATA_STREAM
         * */
        console.log('Saving Location Info!');
        var onNavigatorSuccess = function (position) {
            var doc = position.coords;
            // doc["timestamp"] = position["timestamp"];
            transformer.transform(doc, 'navigator', storeAndDisplayMessages)
        };
        var nvOptions = {
            maximumAge: 3000,
            timeout: 5000,
            enableHighAccuracy: true
        };

        navigator.geolocation.getCurrentPosition(onNavigatorSuccess, logError, nvOptions);

    };

    var sendInfo = function () {
        /**
         * Get an instance of the sensors, perform transformation and store it in data stream,
         * then publish all collected data to remote server
         * */

        saveDeviceInfo();
        console.log('Streaming Device Info!');

        var onPublishSuccess = function (response) {
            $('#response').text(response);
        };

        var onPublishFail = function (request, status, error) {
            console.log("Error status " + status);
            console.log("Error request status: " + request.status);
            console.log("Error request status text: " + request.statusText);
            console.log("Error request response text: " + request.responseText);
            console.log("Error response header: " + request.getAllResponseHeaders());
            logError(error);
        };
        console.log(DATA_STREAM);
        transformer.publish(DATA_STREAM.reverse(),
            onPublishSuccess,
            onPublishFail
        );

        DATA_STREAM.length = 0
    };
});


